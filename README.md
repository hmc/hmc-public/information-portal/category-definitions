# HMC CCT1: Kartierung / Mapping

This repository will be used to work on definitions and schemas used in HMC's CCT1 for mapping research data management resources in the Helmholtz Association.

Work on these definitions needs to be coordinated and synchronized with HMC's glossary group CCT7. 


## Installation:
The is some utility which can be installed as follows

```
git clone https://gitlab.hzdr.de/hmc/hmc/cct-1-mapping/category-definitions.git

cd category-definitions

pip install .
```

## Usage:
Under `hmc_schemas`are json schema files which comply with the CCT1 definitions.
These can be used for validating corresponding (meta)datasets.

There is also some python utility, which makes these schemas useable throughout other packages

```
from hmc_schemas import load_schema

schema = load_schema('data_source')
schema_0_1= load_schema('data_source', version='0.1.0')
```

you can now use the loaded schema for validation using for example the jsonschema package.
```
from jsonschema import validate

data = {}
validate(instance=data, schema=schema_0_1)
```

## Countinous Integration

The CI pipeline checks with each commit if all json files and all yaml files have valid syntax, you can also check this by running 
```
pre-commit run --all-files
```
before you commit something.
In additions it tests the untility.
