
"""Utility CLI for the application."""


import typer
import json
import traceback
import sys
import datetime
from typing import Optional, List
from pathlib import Path
from pprint import pprint
from jsonschema.exceptions import ValidationError, SchemaError
from .schema_util import list_schemas
from .schema_util import load_schema
from .schema_util import validate_with
from jsonschema import validate, exceptions
from jsonschema.validators import Draft7Validator

cli = typer.Typer()

def format_exception(e):
    exception_list = traceback.format_stack()
    exception_list = exception_list[:-2]
    exception_list.extend(traceback.format_tb(sys.exc_info()[2]))
    exception_list.extend(traceback.format_exception_only(sys.exc_info()[0], sys.exc_info()[1]))

    exception_str = "Traceback (most recent call last):\n"
    exception_str += "".join(exception_list)
    # Removing the last \n
    exception_str = exception_str[:-1]

    return exception_str



@cli.command()
def list(
    category: Optional[str]= typer.Option(None, help="The HMC category to be filterd for."),
    version: Optional[str] = typer.Option(None, help='The schema version to be filtered for.')) -> None:
    """List all schemas available"""
    list_schemas(category=category, version=version)

@cli.command()
def validate(
    filenames: List[Path],
    category: Optional[str] = typer.Option(None, help="The HMC category to be validated against."),
    version: Optional[str] = typer.Option(None, help='The schema version to be validated against.'),
    stop: Optional[bool] = typer.Option(False, help='If the validation should stop in first encountered error, sees the full stack track'),
    trace: Optional[bool] = typer.Option(True, help='Print the full stack track for each exception'),
    allerr: Optional[bool] = typer.Option(False, help='Use a piecewise jsonschema evaluation method to see all errors in a file and not the first one only.'),
    outfile: Optional[Path] = typer.Option(Path('.') / 'validation_issues.json', help='filepath to save the error report to.'),
    ) -> None:
    ignoreinternal: Optional[bool] = typer.Option(True, help='Keys starting with _ prior the evaluation.'),
    summary_report: Optional[bool] = typer.Option(True, help='Should a summary of all error be printed at the end?')) -> None:
    """Validate a given json file

    example usage:
    ```
    hmc_schemas validate --category repository ./1_repositories/*.json
    ```
    This will evaluate all json files in the 1_repositories folder against
    the latest repository schema
    """
    '''
    # Typer cannot use this...
    :param filenames: A lists of filepaths to be validated
    :type filenames: List[Path]
    :param category: The HMC category the given jsons corresponds to and should be validated against.
    If not provided it will try to guess them (for now by looking at the '_cat' key)
    :type category: str, optional
    :param version: The version of the schema to be loaded
    :type version: str, optional
    :param summary_report: Should a summary report be printed at the end, default True, if False it will stop on first errror.
    :type summary_report: boolean, optional
    '''
    summary = {}
    for filename in filenames:
        print(f'Validating {filename}')
        with open(filename, 'r') as fileo:
            try:
                json_dict = json.load(fileo)
            except json.decoder.JSONDecodeError as er: # could not read json, no proper json
                count = summary.get(repr(er), 0)
                count = count + 1
                summary[repr(er)] = count
                continue

        if ignoreinternal:
            json_dict_new = {}
            # ignore all keys with '_' for validation
            for key, val in json_dict.items():
                if not key.startswith('_'):
                    json_dict_new[key] = val
            json_dict = json_dict_new


        #print(json_dict)
        if category is None:
            if '_cat' in json_dict.keys():
                category=json_dict['_cat']
            else:
                raise ValueError('No category specified, and could not guess category from data.')
        if not allerr:
            try:
                res = validate_with(json_dict, category=category, version=version)
            except (SchemaError, ValidationError, TypeError) as er:
                if stop:
                    raise er
                #if trace:
                #    print(format_exception(er))
                print(repr(er))
                print(er)
                res = False
                count = summary.get(repr(er), 0)
                count = count + 1
                summary[repr(er)] = count
        else:
            # better would be to include that in validate_with instead....
            schema = load_schema(category, version=version)
            validator = Draft7Validator(schema)
            errList = validator.iter_errors(json_dict)
            for err in errList:
                err = str(err).replace("\n", " ")
                print(err)
                logValidationExceptions("Validation Error", str(err), category, version, filename, filepath=outfile)
            if errList is None:
                res = True
            else:
                res = False
        if res:
            print(f"This file is valid: {filename}")
    if summary_report:
        print('Validation summary, errors: number of occurances')
        pprint(summary)

def logValidationExceptions(errType, message, schematype, schemaVersion, datafilepath, lineNo="", filepath=Path('validation_issues.json')):
    # print("Writing to a file")

    valObjs = []
    if filepath.exists():
        with open(filepath, 'r') as valfile:
            valObjs = json.load(valfile)

    data = {"Document Type": schematype, "Schema Version": schemaVersion, "Document": datafilepath, "Exception Type": errType, "Exception Message": message, "Property or Line no.": lineNo, "Timestamp": datetime.datetime.now()}
    valObjs.append(data)
    with open(filepath, 'w') as valfile2:
        json.dump(valObjs, valfile2, indent=2, default=str)  # 'default' property converts any non-serializable entity into (here) a str

if __name__ == "__main__":  # pragma: no cover
    cli()
