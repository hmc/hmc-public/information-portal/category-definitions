{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "/hmc_schemas/dataset/1_0_2/hmc_dataset.json",
    "type": "object",
    "title": "DRAFT of CCT1 JSON schema datasets",
    "description": "DRAFT of CCT1 properties for datasets. Most of this is taken from the 'openaire result' schema.",
    "required": [
        "author"
    ],
    "allOf": [
    {"$ref": "./../../common_properties/1_1_0/hmc_common_properties.json"}
    ],
    "definitions": {
        "AccessRight":{
            "type":"object",
            "properties":{
              "code": {
                "type": "string",
                "description": "COAR access mode code: http://vocabularies.coar-repositories.org/documentation/access_rights/"
              },
              "label": {
                "type": "string",
                "description": "Label for the access mode"
              },
              "scheme": {
                "type": "string",
                "description": "Scheme of reference for access right code. Always set to COAR access rights vocabulary: http://vocabularies.coar-repositories.org/documentation/access_rights/"
              }
            }
        },
        "ControlledField": {
            "description": "To represent the information described by a scheme and a value in that scheme (i.e. pid)",
            "type": "object",
            "properties": {
                "scheme": {
                  "type": "string"
                },
                "value": {
                  "type": "string"
                }
            }
        }
    },
    "properties": {
        "author": {
            "description": "The authors of the data publication.",
            "type": "array",
            "minItems": 1,
            "items": {
                "type": "object",
                "additionalProperties": false,
                "required": [
                  "fullname",
                  "name",
                  "surname"
                ],
                "properties": {
                    "fullname": {
                        "description": "Fullname of the author",
                        "type": "string",
                        "minLength": 1
                    },
                    "name": {
                        "description": "Name of the author.",
                        "type": "string",
                        "minLength": 1
                    },
                    "surname": {
                        "description": "Surname of the author.",
                        "type": "string",
                        "minLength": 1
                    },
                    "affiliation": {
                        "description": "Affiliation of the author.",
                        "type": "string"
                    },
                    "authorPID": {
                        "description": "IDs which uniquely identify the author like ORCIDs",
                        "type": "object",
                        "properties": {
                          "id": {
                            "allOf": [
                              {"$ref": "#/definitions/ControlledField"}
                            ]
                          }
                        }
                    }
                }
            }
          },
          "bestAccessRight": {
            "type": "object",
            "properties": {
              "code": {
                "type": "string",
                "description": "COAR access mode code: http://vocabularies.coar-repositories.org/documentation/access_rights/"
              },
              "label": {
                "type": "string",
                "description": "Label for the access mode"
              },
              "scheme": {
                "type": "string",
                "description": "Scheme of reference for access right code. Always set to COAR access rights vocabulary: http://vocabularies.coar-repositories.org/documentation/access_rights/"
              }
            },
            "description": "The openest access right associated to the manifestations of this research results"
        },
        "contributor": {
            "type": "array",
            "items": {
              "type": "string",
              "description": "Description of contributor"
            }
        },
        "relatedPublications": {
            "type": "array",
            "items": {
              "type": "string",
              "description": "Description of contributor"
            }
        },
        "country": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "code": {
                  "type": "string",
                  "description": "ISO 3166-1 alpha-2 country code"
                },
                "label": {
                  "type": "string"
                }
              }
            }
        },
        "dataFormat": {
            "type": "array",
            "items": {
              "type": "string"
            }
        },
        "geolocation": {
            "description": "Geolocation information",
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "box": {
                  "type": "string"
                },
                "place": {
                  "type": "string"
                },
                "point": {
                  "type": "string"
                }
              }
            }
        },
        "publisher": {
            "type": "string"
        },
        "fairScore": {
            "description": "Fair score evaluation via F-UJI",
            "type": "object",
            "items": {
                "version": {
                    "type": "string",
                    "description": "Fuji version with which the assessment was done"
                },
                "assessmentDate": {
                    "type": "string",
                    "description": "Date the assessment was run"
                },
                "totalScore": {
                    "description": "Total FAIR Score in percent",
                    "type": "float"
                },
                "FScore": {
                    "description": "Score for F in percent",
                    "type": "float"
                },
                "AScore": {
                    "description": "Score for A in percent",
                    "type": "float"
                },
                "IScore": {
                    "description": "Score for I in percent",
                    "type": "float"
                },
                "RScore": {
                    "description": "Score for R in percent",
                    "type": "float"
                }
            }
          },
        "size": {
          "type": "string",
          "description": "Only for results with type 'dataset': the declared size of the dataset"
        },
        "source": {
          "description": "See definition of Dublin Core field dc:source",
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "tool": {
          "description": "Only for results with type 'other': tool useful for the interpretation and/or re-used of the research product",
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "dataType": {
          "type": "string",
          "description": "Type of the result: one of 'publication', 'dataset', 'software', 'other' (see also https://api.openaire.eu/vocabularies/dnet:result_typologies). In our case Software has a separate schema",
          "enum": [
            "publication",
            "dataset",
            "other"
          ]
        }
    },
    "propertyNames": {
        "enum": ["mappingCategory", "documentationUri", "date", "keyword", "resourceUri", "scientificDiscipline", "resourceName", "contact", "helmholtzResearchField", "identifier", "resourceDescription", "resourceStatus",  "author",
            "dataType", "contributor", "publisher", "fairScore", "country", "accessRight", "size", "tool", "source", "geolocation", "dataFormat", "relatedPublication"]
    }
}
