"""
Module containing utilites to deal with the schema files and different version

We could also add something to always combine the subschemas with the common properties, if we want.
"""
import os
import platform
import json
from typing import Optional
from jsonref import JsonRef

# flag whether we are on windows
_ON_WINDOWS = platform.system() == "Windows"
PACKAGE_DIRECTORY = os.path.dirname(os.path.abspath(__file__))

# Schema list should be sorted by verison number, latest version last element in list, lists contain (relative filepath, version)
# Currently the version is in the filename, but we do not parse this for now. Or a nested dict....
# Or better move to a file and read from there, so that is it could be used outside of python

# Register Schema files here
registry_path = os.path.abspath(os.path.join(PACKAGE_DIRECTORY, 'schema_registry.json'))
if os.path.exists(registry_path):
    with open(registry_path, 'r') as fileo:
        SCHEMAREGISTRY = json.load(fileo)
else: # fallback for now because there are situations where the file is not packed..
    SCHEMAREGISTRY = {"common_properties": [("hmc_common_properties.json", "1.1.0")],
        "data_format": [("hmc_data_format.json", "1.1.0")],
        "dataset": [("hmc_dataset.json", "1.0.2")],
        "data_source": [("hmc_data_source.json", "1.0.2")],
        "document": [("hmc_document.json", "1.0.2")],
        "license": [("hmc_license.json", "1.0.2")],
        "link": [("hmc_link.json", "1.0.2")],
        "metadata_standard": [("hmc_metadata_standard.json", "1.0.2")],
        "method": [("hmc_method.json", "1.0.2")],
        "organization": [("hmc_organization.json", "1.1.0")],
        "pid_system": [("hmc_pid_system.json", "1.0.2")],
        "policy": [("hmc_policy.json", "1.0.2")],
        "record_metadata": [("hmc_record_metadata.json", "1.0.1")],
        "repository": [("hmc_repository.json", "1.0.2")],
        "schema_crosswalk": [("hmc_schema_crosswalk.json", "1.0.2")],
        "software": [("hmc_software.json", "1.0.2")],
        "terminology": [("hmc_terminology.json", "1.0.2")],
        "training_material": [("hmc_training_material.json", "1.0.2")]
        }


def list_schemas(
    category: str = None, version: str = None, verbose: bool = True
) -> list:
    """List schemas from the SCHEMAREGISTRY and ggf applies filters

    :param category: Provide a category to filter for, defaults to None
    :type category: [str], optional
    :param version: provide a version to filter for, defaults to None
    :type version: [str], optional
    :param verbose: If verbose is true prints the output, defaults to True
    :type verbose: bool, optional
    :return: returns a list of all schema files of a certain category and version
    :rtype: list
    """
    # Returning a dict instead may be more useful?
    schema_list_t = []
    output_list = []
    for cat, schemalist in SCHEMAREGISTRY.items():
        if category is not None:
            if category == cat:
                schema_list_t.extend(schemalist)
        else:
            schema_list_t.extend(schemalist)

    for schema, version_s in schema_list_t:
        if version is not None:
            if version == version_s:
                output_list.append((schema, version_s))
        else:
            output_list.append((schema, version_s))

    if verbose:
        output_string = " ".join(
            [f"{version_s} : {schema}\n" for schema, version_s in output_list]
        )
        print(output_string)
    return output_list


def get_schema_path(category: str, version: str = None) -> Optional[str]:
    """Get the absolute path to a schema file for a certain category with the latest version

    or a specifc verison.
    :param category: [description], defaults to None
    :type category: str
    :param version: [description], defaults to None
    :type version: str, optional
    :return: [description]
    :rtype: str
    """
    # TODO make this work with windows through Pathlib

    schemas = list_schemas(category=category, version=version, verbose=False)
    if len(schemas) == 0:
        return None
    else:
        latest_version = str(schemas[-1][1]).replace('.', '_')
        schema_path = os.path.abspath(
            os.path.join(PACKAGE_DIRECTORY, str(category), latest_version, str(schemas[-1][0]))
        )

    return schema_path


def path_to_file_uri(path: str) -> str:
    """Convert an absolute path into a valid file:///... URI."""
    if _ON_WINDOWS:
        p = path.replace('\\', '/')
        return f"file:///{p}"
    else:
        return f"file://{path}"


def load_schema(category: str, version: str = None) -> Optional[dict]:
    """Opens a schemafile for a certain category, per default the last version

    Specify the version to open a schema of that category with a different version.

    :param category: [description], defaults to None
    :type category: str
    :param version: [description], defaults to None
    :type version: str, optional
    :return: Returns the loaded schema file as a dict, or None if none found for that
    :rtype: dict
    """
    schemafile = get_schema_path(category=category, version=version)
    if schemafile is None:
        return None

    with open(schemafile, "r", encoding="utf-8") as fileo:
        schema = json.load(fileo)
    schema = JsonRef.replace_refs(
        schema, jsonschema=True, base_uri=path_to_file_uri(schemafile)
    )

    return schema


def validate_with(jsondict, category: str, version: str = None):
    """Validate a given dict with a schema file from a specified category

    :param jsondict: A dictionary to validate
    :type jsondict: dict
    :param category: The hmc category
    :type category: str
    :param version: The schema version, defaults to None, i.e latest
    :type version: str, optional

    :raises: jsonschema.exceptions.ValidationError
    :raises: jsonschema.exceptions.SchemaError
    """
    from jsonschema import validate

    schema = load_schema(category=category, version=version)
    if schema is None:
        print('No schema loaded....')
    validate(instance=jsondict, schema=schema)
    # if it does not validate it would throw an error
    return True
