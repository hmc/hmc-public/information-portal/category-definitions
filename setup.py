# -*- coding: utf-8 -*-
"""
setup: usage: pip install -e .[graphs]
"""

from setuptools import setup, find_packages
import io  # needed to have `open` with encoding option

# read the contents of your README file
from os import path

this_directory = path.abspath(path.dirname(__file__))
with io.open(path.join(this_directory, 'README.md'), encoding='utf8') as f:
    long_description = f.read()

if __name__ == '__main__':
    setup(
        name='hmc_schemas',
        version='1.0.1',
        description='Contains the HMC category definitions and correcponding schema files',
        # add long_description from readme.md:
        long_description = long_description, # add contents of README.md
        long_description_content_type ='text/markdown',  # This is important to activate markdown!
        url='https://gitlab.hzdr.de/hmc/hmc/cct-1-mapping/category-definitions/',
        author='HMC',
        author_email='hmc-info@geomar.de',
        license='Apache License 2.0, see LICENSE file.',
        classifiers=[
            'Development Status :: 1 - Alpha',
            'Intended Audience :: Science/Research',
            'License :: OSI Approved :: Apache Software License',
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.7',
            'Programming Language :: Python :: 3.8',
            'Programming Language :: Python :: 3.9',
        ],
        keywords='metadata, schema, json schema, fair data',
        packages=find_packages(exclude=['tests*']),
        include_package_data=True,
        install_requires=[
            'jsonschema==3.2.0',
            'jsonref<1.0.0',
            'typer'
        ],
        extras_require={
            'testing': [
                'pytest-cov >= 2.5.0',
                'pytest'],
            'pre-commit': [
                'pre-commit'
            ]
        },
        entry_points= {
            "console_scripts": [
                'hmc_schemas = hmc_schemas.cli:cli'
            ]
        },
    )
