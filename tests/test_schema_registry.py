
import pytest
from hmc_schemas import SCHEMAREGISTRY
from hmc_schemas import list_schemas
from hmc_schemas import get_schema_path
from hmc_schemas import load_schema

CATEGORIES = [(key, schema[-1][1]) for key, schema in SCHEMAREGISTRY.items()]

@pytest.mark.parametrize('category', CATEGORIES)
def test_load_schema_latest(category):
    """Test if load schema returns a valid json/dict
    and if it is the same as the latest verison
    """

    #from pprint import pprint
    schema = load_schema(category[0])
    schema2 = load_schema(category[0], version=category[1])
    #if category == 'record_metadata':
    #    pprint(schema)
    #    print(list(schema['properties'].keys()))

    assert isinstance(schema, dict)
    assert isinstance(schema2, dict)

    assert schema == schema2 # This is only for now...
