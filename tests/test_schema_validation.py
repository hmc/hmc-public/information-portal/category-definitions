import os
import json
import pytest
from hmc_schemas import SCHEMAREGISTRY
from hmc_schemas import load_schema #, validate_with
from jsonschema import validate


TEST_DIR = os.path.dirname(os.path.abspath(__file__))

# This auto discovers files, but only named test.json and within the supported folder structure
schemas_data = []
for key, schemas in SCHEMAREGISTRY.items():
    for schema in schemas:
        version = schema[1]
        version = version.replace('.', '_')
        # for now we assume one test file per version
        testdata_path = os.path.abspath(os.path.join(TEST_DIR, f'./data/{key}/{version}/test.json'))
        if os.path.exists(testdata_path):
            schemas_data.append(((key, schema[1]), testdata_path))
        else:
            print(testdata_path)

#print(schemas_data)
#schemas_data = [(('metadata_standard', '1.0.0'), './schema_registry.json')]
@pytest.mark.parametrize('schema_data', schemas_data)
def test_data_validation_tests(schema_data):
    """
    Load a given schema and tries to validate a given full test data instance


    schema_data : ((category, version), datainstance_file_path)
    Throws a jsonschema.exceptions.SchemaError if schema is invalid
    or a jsonschema.exceptions.ValidationError if the validation is not sucessfull
    """
    print(schema_data)
    schemad = schema_data[0]
    datafilepath = schema_data[1]

    with open(datafilepath, 'r') as fileo:
        jsondict = json.load(fileo)

    # get right schema
    schema = load_schema(schemad[0], version=schemad[1])
    #print(schema)
    # ignore all keys with '_' for validation
    datat = {}
    for key, val in jsondict.items():
        if not key.startswith('_'):
            datat[key] = val

    validate(instance=datat, schema=schema) # Will throw errors if not
    #validate_with(datat, category=schema[0], version=schema[1])
